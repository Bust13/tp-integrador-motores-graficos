using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Niveles : MonoBehaviour
{
   
    public void Nivel1()
    {

        SceneManager.LoadScene(1);
        Time.timeScale = 1;
    }
    public void Nivel2()
    {

        SceneManager.LoadScene(2);
        Time.timeScale = 1;
    }
    public void Nivel3()
    {

        SceneManager.LoadScene(3);
        Time.timeScale = 1;
    }

}
