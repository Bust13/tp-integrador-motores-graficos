using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
public class ControlEnemigoNivel1 : MonoBehaviour
{
    public TextMeshProUGUI textoGameOver;
    public Image pantallaNegra;
    public Button botonReiniciar;
    public Button botonVolveralMenu;
    public GameObject[]puntos;
    public TextMeshProUGUI interfaz;
    public float velocidadDePlataforma=2;
    int puntosIndex = 0;

    

    void Update()
    {
        MoverPlataforma();

    }
    void MoverPlataforma() { 
    
    if (Vector3.Distance(transform.position, puntos[puntosIndex].transform.position) < 0.1f)
        {

            puntosIndex++;
            if (puntosIndex >= puntos.Length)
            {
                puntosIndex = 0;

            }

        }
        transform.position = Vector3.MoveTowards(transform.position,puntos[puntosIndex].transform.position,velocidadDePlataforma*Time.deltaTime);
    
    }


    
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemigo"))
        {
            Destroy(other.gameObject);
        }
        else if (other.gameObject.CompareTag("jugador"))
        {
            textoGameOver.gameObject.SetActive(true);
          
            Cursor.lockState = CursorLockMode.None;
            pantallaNegra.gameObject.SetActive(true);
            interfaz.gameObject.SetActive(false);
            botonReiniciar.gameObject.SetActive(true);
            botonVolveralMenu.gameObject.SetActive(true);
           
        }
    }

}


