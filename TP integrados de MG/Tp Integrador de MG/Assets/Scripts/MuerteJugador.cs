using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
public class MuerteJugador : MonoBehaviour
{
    public TextMeshProUGUI textoGameOver;
    public Image pantallaNegra;
    public Button botonReiniciar;
    public Button botonReiniciarJuego;
    public Button volerMenu;
    public TextMeshProUGUI interfaz;

    void Start()
    {

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bala"))
        {

            textoGameOver.gameObject.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
            pantallaNegra.gameObject.SetActive(true);
            Time.timeScale = 0;
            botonReiniciar.gameObject.SetActive(true);
            volerMenu.gameObject.SetActive(true);
            interfaz.gameObject.SetActive(false);

        }
    }
}
