using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonidoPaso : MonoBehaviour
{
    public AudioSource paso;

    void OnTriggerEnter (Collider other)
    {
        if (other.gameObject.CompareTag("Piso")){

            paso.Play();
        }

    }
}
