


using UnityEngine;
using UnityEngine.SceneManagement;


public class ControlJugador : MonoBehaviour

{
    Rigidbody rb;
    float xInicial, yInicial, zInicial;
    public int maximoDeSaltos = 2;
    public int salto = 0;
    public float fuerza = 6f;
    public float rapidezDesplazamiento = 10f;
    public bool detectaPiso;
    public Camera camaraPrimeraPersona;
    private AudioSource audioSource;
    public bool agacharse;
    public bool sprint;
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
        xInicial = transform.position.x;
        yInicial = transform.position.y;
        zInicial = transform.position.z;
    }

    void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;

        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);


        if (Input.GetKeyDown(KeyCode.Z))
        {
            sprint=true;
        }
        if (Input.GetKeyUp(KeyCode.Z))
        {
            sprint = false;
        }
        if (sprint)
        {
            rapidezDesplazamiento = 20;

        }
        else
        {
            rapidezDesplazamiento = 10;

        }

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }
        Vector3 Piso = transform.TransformDirection(Vector3.down);
        if ((Input.GetButtonDown("Jump")) && (salto > 0))
        {
            rb.AddForce(Vector3.up * fuerza, ForceMode.Impulse);
            salto -= 1;
            audioSource = GetComponent<AudioSource>();
            audioSource.Play();
        }
       
        

        if (Input.GetKeyDown(KeyCode.C))
        {

            agacharse = true;


        }
        if (Input.GetKeyUp(KeyCode.C))
        {

            agacharse = false;


        }

        if (agacharse)
        {
            rb.transform.localScale = new Vector3(2, 2, 2);

        }
        else
        {
            rb.transform.localScale = new Vector3(3, 3, 3);

        }


        


    }

    
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Piso")
        {
            detectaPiso = true;
            salto = maximoDeSaltos;


        } 





    }
    public void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Piso")
        {
            detectaPiso = false;



        }



    }












}
