using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformController : MonoBehaviour
{

    public Rigidbody plataformaRB;
    public Transform[] posicionPlataforma;
    public float velocidadDePlataforma;

    private int posicionActual=0;
    private int proximaPos = 1;

    public bool moverAlaproxima = true;
    public float tiempodeEspera;

    void Update()
    {
        MoviminetoPlataforma();
        
    }

    public void MoviminetoPlataforma()
    {
        if (moverAlaproxima)
        {
            StopCoroutine(EsperarParaMover(0));
            plataformaRB.MovePosition(Vector3.MoveTowards(plataformaRB.position, posicionPlataforma[proximaPos].position, velocidadDePlataforma * Time.deltaTime));
        
        }
        if (Vector3.Distance(plataformaRB.position, posicionPlataforma[proximaPos].position) <= 0)
       
        {
            StartCoroutine(EsperarParaMover(tiempodeEspera));
            posicionActual = proximaPos;
            proximaPos++;

            if (proximaPos > posicionPlataforma.Length - 1)
            {
                proximaPos = 0;

            }

        }

    }

    IEnumerator EsperarParaMover(float time)
    {
        moverAlaproxima = false;
        yield return new WaitForSeconds(time);
        moverAlaproxima = true;


    }

}
