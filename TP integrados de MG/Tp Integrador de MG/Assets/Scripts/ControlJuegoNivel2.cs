using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class ControlJuegoNivel2 : MonoBehaviour
{
    public void reiniciarEscena()
    {
        SceneManager.LoadScene(1);
        Time.timeScale = 1;
    } 
    public void reiniciarNivel()
    {
        SceneManager.LoadScene(2);
        Time.timeScale = 1;
    }
    public void volverAlMenu()
    {
        SceneManager.LoadScene("Menu PrincipaL");

    }
}
