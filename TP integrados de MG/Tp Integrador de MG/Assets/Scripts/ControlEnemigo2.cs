using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
public class ControlEnemigo2 : MonoBehaviour
{
    public TextMeshProUGUI textoGameOver;
    public Image pantallaNegra;
    public Button botonReiniciar;
    public Button botonReiniciarNivel;
    public Button botonVolveralMenu;
    public TextMeshProUGUI interfaz;
    public GameObject[] puntos;

    public float velocidadDePlataforma = 2;
    int puntosIndex = 0;



    void Update()
    {
        MoverPlataforma();

    }
    void MoverPlataforma()
    {

        if (Vector3.Distance(transform.position, puntos[puntosIndex].transform.position) < 0.1f)
        {

            puntosIndex++;
            if (puntosIndex >= puntos.Length)
            {
                puntosIndex = 0;

            }

        }
        transform.position = Vector3.MoveTowards(transform.position, puntos[puntosIndex].transform.position, velocidadDePlataforma * Time.deltaTime);

    }



    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemigo"))
        {
            Destroy(other.gameObject);
        }
        else if (other.gameObject.CompareTag("jugador"))
        {
            textoGameOver.gameObject.SetActive(true);

            Cursor.lockState = CursorLockMode.None;
            pantallaNegra.gameObject.SetActive(true);
            botonReiniciarNivel.gameObject.SetActive(true);
            botonReiniciar.gameObject.SetActive(true);
            botonVolveralMenu.gameObject.SetActive(true);
            interfaz.gameObject.SetActive(false);
        }
    }

}


