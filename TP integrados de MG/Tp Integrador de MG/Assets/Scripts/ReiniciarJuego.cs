using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReiniciarJuego : MonoBehaviour
{
    public void reiniciarEscena()
    {
        SceneManager.LoadScene(1);
        Time.timeScale=1;
    }

    public void volverAlMenu()
    {
        SceneManager.LoadScene("Menu PrincipaL");

    }
}
