using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class Ganaste : MonoBehaviour
{
    public TextMeshProUGUI textoGanaste;
    public Image pantallaBlanca;
    public Button botonReiniciar;
    public Button VolveralMenu;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemigo"))
        {
            Destroy(other.gameObject);
        }
        else if (other.gameObject.CompareTag("jugador"))
        {
            textoGanaste.gameObject.SetActive(true);

            Cursor.lockState = CursorLockMode.None;
            pantallaBlanca.gameObject.SetActive(true);
            Time.timeScale = 0;
            botonReiniciar.gameObject.SetActive(true);
            VolveralMenu.gameObject.SetActive(true);
        }

    }

}

