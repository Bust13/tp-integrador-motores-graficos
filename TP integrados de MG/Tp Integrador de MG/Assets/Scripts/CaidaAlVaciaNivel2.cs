using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
public class CaidaAlVaciaNivel2 : MonoBehaviour
{
    public TextMeshProUGUI textoGameOver;
    public Image pantallaNegra;
    public Button botonReiniciar;
    public Button botonReiniciarNivel;
    public Button VolveralMenu;
    public TextMeshProUGUI interfaz;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.CompareTag("Enemigo"))
        {
            Destroy(other.gameObject);
        }
        else if (other.gameObject.CompareTag("jugador"))
        {
            textoGameOver.gameObject.SetActive(true);
           
            Cursor.lockState = CursorLockMode.None;
            pantallaNegra.gameObject.SetActive(true);
            Time.timeScale = 0;
            botonReiniciar.gameObject.SetActive(true);
            botonReiniciarNivel.gameObject.SetActive(true);
            VolveralMenu.gameObject.SetActive(true);
            interfaz.gameObject.SetActive(false);
        }

    }
}
